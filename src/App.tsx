import React from "react";
import { BrowserRouter } from "react-router-dom";

import { Root } from "./pages/Root";

import { store } from "./stores";
import { Provider } from "react-redux";
import Loading from "./components/Loading";

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Root />

        <Loading />
      </BrowserRouter>
    </Provider>
  );
}

export default App;
