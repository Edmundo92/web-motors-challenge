import React from "react";
import { VehicleProps } from "../stores/vehicle/vehicle-types";

const Card = ({ vehicle }: { vehicle: VehicleProps }) => {
  return (
    <div className="card-container">
      <img
        src={vehicle.Image}
        alt={vehicle.Make}
        className="card-container-image"
      />

      <div className="card-inf">
        <div className="mark-model">
          <strong>{vehicle.Make}</strong>
          <span>{vehicle.Model}</span>
        </div>

        <p className="price">R$ {vehicle.Price}</p>

        <div className="year-km-container">
          <div className="year-f-m">
            <span>{vehicle.YearFab}</span>/<span>{vehicle.YearModel}</span>
          </div>
          <span className="km">{vehicle.KM} km</span>
        </div>
      </div>
    </div>
  );
};

export default Card;
