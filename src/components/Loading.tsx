import React, { useEffect } from "react";
import { connect } from "react-redux";

const Loading = ({ loading }: any) => {
  useEffect(() => {}, [loading]);

  return loading.isLoading ? (
    <div className="loading-container">
      <img src={require("../assets/images/load.gif")} alt="Load" />
    </div>
  ) : null;
};

const mapStateToProps = ({ loading }: any) => ({ loading });

export default connect(mapStateToProps, null)(Loading);
