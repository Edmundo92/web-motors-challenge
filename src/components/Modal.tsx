import React from "react";

import { useTransition, animated } from "react-spring";

type ModalType = {
  onCancel: () => void;
  isShow: boolean;
  showDataInModal: any;
};

type ShotProp = {
  images: ImagesProp;
};

type ImagesProp = {
  normal: string;
  hidpi: string;
};

const Modal = ({ isShow, onCancel, showDataInModal }: ModalType) => {
  const data = showDataInModal;

  const props = useTransition(isShow, null, {
    from: { opacity: 0, marginTop: "-50px" },
    enter: { opacity: 1, marginTop: "0px" },
    leave: { opacity: 0, marginTop: "150px" }
  });

  const propsWrapper = useTransition(isShow, null, {
    from: { opacity: 0 },
    enter: { opacity: 0.4 },
    leave: { opacity: 0 }
  });

  return (
    <React.Fragment>
      {propsWrapper.map(({ item, styles, key }: any) => {
        return item ? (
          <div key={key} className="modal-wrapper">
            {props.map(({ item, props, key }: any) => {
              return item ? (
                <animated.div
                  key={key}
                  style={props}
                  className="modal__content"
                >
                  {/* start children */}
                  <div className="modal-wrapper-children">
                    <header>
                      <span onClick={onCancel}>X</span>
                    </header>
                    <main>
                      <img src={data.Image} alt="" />
                    </main>
                    <footer></footer>
                  </div>
                </animated.div>
              ) : null;
            })}
          </div>
        ) : null;
      })}
    </React.Fragment>
  );
};

export default Modal;
