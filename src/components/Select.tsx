import React, { useState } from "react";
import { FiChevronDown } from "react-icons/fi";

const Select = ({ name, data, handleChange, placeholder, isDisabled }: any) => {
  const [isOpen, setIsOpen] = useState(false);
  const [value, setValue] = useState<any>("");

  function openOptions() {
    setIsOpen(!isOpen);
  }

  function handleChangeValue(value: any) {
    setValue(value);
    const v = {
      ...value,
      name
    };
    handleChange(v);
    openOptions();
  }

  return (
    <div className="select-container">
      <div className="custom-select">
        <FiChevronDown
          color="black"
          className="arrow-down-icon"
          onClick={openOptions}
        />
        <input type="hidden" name={name} />
        <div
          className={
            isOpen ? "select-selected select-arrow-active" : "select-selected"
          }
          onClick={openOptions}
        >
          {placeholder && (
            <label htmlFor="" style={{ marginBottom: 0, marginRight: "8px" }}>
              {placeholder}:
            </label>
          )}
          <span>{value.Name}</span>
        </div>
        <div className={isOpen ? "select-items" : "select-items select-hide"}>
          {data &&
            data.map((el: any) => (
              <div
                key={el.ID}
                onClick={() => handleChangeValue(el)}
                style={
                  el === value ? { backgroundColor: "rgba(0,0,0,.1)" } : {}
                }
              >
                {el.Name}
              </div>
            ))}
        </div>
      </div>
      {isDisabled && <div className="disabled-select"></div>}
    </div>
  );
};

export default Select;
