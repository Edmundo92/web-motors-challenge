import React, { useState, cloneElement } from "react";
import classnames from "classnames/bind";

export const TabContainer = (props: any) => {
  const { className } = props;
  const [activeIndex, setActiveIndex] = useState(props.defaultActiveIndex || 0);
  const handleTabClick = (tabIndex: any) => {
    if (tabIndex !== activeIndex) {
      setActiveIndex(tabIndex);
    }
  };

  const cloneTabElement = (tab: any, index = 0) => {
    return cloneElement(tab, {
      onClick: () => handleTabClick(index),
      tabIndex: index,
      isActive: index === activeIndex
    });
  };

  const renderChildrenTabs = () => {
    const { children } = props;

    if (!Array.isArray(children)) {
      return cloneTabElement(children);
    }

    return children.map(cloneTabElement);
  };

  const renderActiveTabContent = () => {
    const { children } = props;

    if (children[activeIndex]) {
      return children[activeIndex].props.children;
    }

    return children[activeIndex].props.children;
  };

  return (
    <section className={`tabs-c ${className}`}>
      <div className="tab-logo">
        <img src={require("../assets/images/webmotors.svg")} alt="" />
      </div>
      <div className="tab-header">
        <ul className={"tabs__list"}>{renderChildrenTabs()}</ul>
        <button className="buy-car">Comprar</button>
      </div>
      <div className={"tabs__content"}>{renderActiveTabContent()}</div>
    </section>
  );
};

export const TabItem = (props: any) => {
  const cx = classnames;
  const { className, isActive, onClick, buyType, icon } = props;
  const cssTabClass = cx("tabs__tab", className);
  const activeLinkClass = cx("tab-item tabs__tab-link", {
    // eslint-disable-next-line
    ["tabs__tab-link--active"]: isActive
  });

  return (
    <li key={buyType} className={cssTabClass}>
      <div className={activeLinkClass} onClick={onClick}>
        <span className="active-item-tab">{icon}</span>
        <div className="label">
          <span className="small">COMPRAR</span>
          <span className="big active-item-tab">{buyType}</span>
        </div>
      </div>
    </li>
  );
};

TabContainer.TabItem = TabItem;
