import React, { useEffect, useState, SyntheticEvent } from "react";
import { connect } from "react-redux";
import { Dispatch, bindActionCreators } from "redux";
import Select from "../../components/Select";

// actions
import { modelAction } from "../../stores/model/model-action";
import { versionAction } from "../../stores/version/version-action";

import { FormSearchPropsOrState } from "./home-types";

const FormSearch = ({
  markList,
  modelList,
  versionList,
  modelAction,
  versionAction
}: any) => {
  const [form, setForm] = useState({
    make: null,
    model: null,
    price: null,
    year: null,
    version: null
  });

  useEffect(() => {}, [markList, modelList]);

  function handleChange(obj: any) {
    if (obj.name === "mark") {
      modelAction(obj.ID);
    }

    if (obj.name === "model") {
      versionAction(obj.ID);
    }

    setForm((old) => ({ ...old, [obj.name]: obj.ID }));
  }

  function handleReset(e: SyntheticEvent) {
    e.preventDefault();
    setForm({
      make: null,
      model: null,
      price: null,
      year: null,
      version: null
    });
  }

  function handleSubmit(e: SyntheticEvent) {
    e.preventDefault();
  }

  return (
    <div className="form-container">
      <form onReset={handleReset} onSubmit={handleSubmit}>
        <div className="checkbox-group">
          <div className="check-item">
            <input type="checkbox" name="novos" value="novos" />
            <label htmlFor="">Novos</label>
          </div>

          <div className="check-item">
            <input type="checkbox" name="usados" value="usados" />
            <label htmlFor="">Usados</label>
          </div>
        </div>

        <div className="select-group">
          <div className="mg-r">
            <Select name="state" isDisabled={true} />
          </div>

          <div className="select-group-items">
            <div className="mg-r">
              <Select
                placeholder="Marca"
                data={markList.length > 0 ? markList : []}
                name="mark"
                handleChange={handleChange}
              />
            </div>

            <div>
              <Select
                placeholder="Modelo"
                data={modelList.length > 0 ? modelList : []}
                name="model"
                handleChange={handleChange}
                isDisabled={modelList.length > 0 ? false : true}
              />
            </div>
          </div>
        </div>

        <div className="select-group">
          <div className="select-group-items mg-r">
            <div className="mg-r">
              <Select
                placeholder="Ano desejado"
                name="year"
                isDisabled={true}
              />
            </div>

            <div>
              <Select
                placeholder="Faixa de preço"
                name="price"
                isDisabled={true}
              />
            </div>
          </div>

          <div>
            <Select
              placeholder="Versão"
              name="version"
              data={versionList.length > 0 ? versionList : []}
              handleChange={handleChange}
              isDisabled={versionList.length > 0 ? false : true}
            />
          </div>
        </div>

        <div className="button-container">
          <div className="more-action">
            <span className="arrow">{">"}</span>{" "}
            <span className="info">Busca Avançada</span>
          </div>

          <div className="buttons">
            <button className="reset" type="reset">
              Limpar Filtros
            </button>

            <button className="submit" type="submit">
              VER OFERTAS
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

const mapStateToProps = ({
  modelList,
  versionList,
  markList
}: FormSearchPropsOrState) => {
  return {
    modelList,
    versionList,
    markList
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return bindActionCreators({ versionAction, modelAction }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(FormSearch);
