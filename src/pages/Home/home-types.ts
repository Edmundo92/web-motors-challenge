import { MarkProps } from "../../stores/mark/mark-types";
import { VehicleProps } from "../../stores/vehicle/vehicle-types";
import { ModelProps } from "../../stores/model/model-types";
import { VersionProps } from "../../stores/version/version-types";

export type HomeStateOrProps = {
  vehicleList: VehicleProps[];
  vehicleAction: (markId?: number) => void;
  markAction: () => void;
};

export type FormSearchPropsOrState = {
  markList?: MarkProps[];
  versionList?: VersionProps[];
  modelList?: ModelProps[];
  modelAction?: (markId?: number) => void;
  versionAction?: (modelId?: number) => void;
};
