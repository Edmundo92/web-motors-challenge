import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";

import FormSearch from "./FormSearch";

import Card from "../../components/Card";
import { HomeStateOrProps } from "./home-types";

// actions
import { markAction } from "../../stores/mark/mark-action";
import { vehicleAction } from "../../stores/vehicle/vehicle-action";

//type
import { VehicleProps } from "../../stores/vehicle/vehicle-types";
import { FiArchive } from "react-icons/fi";
import { TabContainer } from "../../components/TabContainer";
import { FaCarAlt, FaMotorcycle } from "react-icons/fa";
import Modal from "../../components/Modal";

const Home = ({ vehicleAction, markAction, vehicleList }: HomeStateOrProps) => {
  const [isShowModal, setIsShowModal] = useState(false);
  const [dataInModal, setDataInModal] = useState<VehicleProps>();

  useEffect(() => {
    vehicleAction();
  }, [vehicleAction]);

  useEffect(() => {
    markAction();
  }, [markAction]);

  function showDataInModal(data: VehicleProps) {
    setIsShowModal(true);
    setDataInModal(data);
  }

  return (
    <>
      <div className="container">
        <div className="home-container">
          <TabContainer>
            <TabContainer.TabItem
              key="buy-car"
              icon={<FaCarAlt />}
              buyType="CARROS"
            >
              <FormSearch />
            </TabContainer.TabItem>
            <TabContainer.TabItem
              key="buy-moto"
              icon={<FaMotorcycle />}
              buyType="MOTOS"
            >
              <div className="motorcycle-conteiner"></div>
            </TabContainer.TabItem>
          </TabContainer>
        </div>

        <div className="show-result-content">
          {vehicleList.length > 0 ? (
            <div className="wm-row">
              {vehicleList.map((vehicle: VehicleProps, index: number) => {
                return (
                  <div key={index} onClick={() => showDataInModal(vehicle)}>
                    <Card vehicle={vehicle} />
                  </div>
                );
              })}
            </div>
          ) : (
            <div className="without-data">
              <div className="">
                <FiArchive className="no-data-icon" />
                <h1>Não existe nenhum dado cadastrado no momento</h1>
                <span>
                  Clique <strong onClick={() => vehicleAction()}>aqui</strong>{" "}
                  para recarregar
                </span>
              </div>
            </div>
          )}
        </div>
      </div>
      {isShowModal && (
        <Modal
          showDataInModal={dataInModal}
          isShow={isShowModal}
          onCancel={() => setIsShowModal(false)}
        />
      )}
    </>
  );
};

const mapStateToProps = ({ vehicleList }: HomeStateOrProps) => ({
  vehicleList
});
const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ vehicleAction, markAction }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
