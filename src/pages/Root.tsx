import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./Home/home";

export const Root = () => (
  <Switch>
    <Route path="/" component={Home} />
  </Switch>
)