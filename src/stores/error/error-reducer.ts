import { ERROR_TYPES, ErrorProps } from "./error-types";

const initialState: ErrorProps = {
  msg: {},
  status: null
};

type ActionProps = {
  type: string;
  payload: any;
};

function error(state = initialState, action: ActionProps) {
  switch (action.type) {
    case ERROR_TYPES.GET_ERRORS:
      return {
        msg: action.payload.msg,
        status: action.payload.status
      };
    default:
      return state;
  }
}

export default error;
