export const ERROR_TYPES = {
  GET_ERRORS: "GET_ERRORS"
};

export type ErrorProps = {
  msg: object;
  status: null;
};
