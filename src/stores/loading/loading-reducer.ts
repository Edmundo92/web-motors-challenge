import { TYPES_LOADING, LoadingProps } from "./loading-types";

const INITIAL_STATE: LoadingProps = {
  loading: {
    isLoading: false
  }
};

type ActionProps = {
  type: string;
  payload: boolean;
};

function loading(state = INITIAL_STATE, action: ActionProps) {
  switch (action.type) {
    case TYPES_LOADING.LOADING:
      return { isLoading: action.payload };
    default:
      return state;
  }
}

export default loading;
