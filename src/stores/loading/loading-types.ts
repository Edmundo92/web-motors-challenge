export const TYPES_LOADING = {
  LOADING: "LOADING"
};

export interface LoadingProps {
  loading?: {
    isLoading?: boolean | null;
  };
}

export interface StateLoadingProps extends LoadingProps {}
