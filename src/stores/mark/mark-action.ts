import { Dispatch } from "redux";
import { MARK_TYPES } from "./mark-types";
import api from "../../services/api";
import { TYPES_LOADING } from "../loading/loading-types";

export function markAction() {
  let isLoading = true;

  return (dispatch: Dispatch) => {
    dispatch({ type: TYPES_LOADING.LOADING, payload: isLoading });
    api
      .get("/OnlineChallenge/Make")
      .then((res) => {
        isLoading = false;
        dispatch({ type: TYPES_LOADING.LOADING, payload: isLoading });
        dispatch({
          type: MARK_TYPES.GET_MARK,
          payload: res.data
        });
      })
      .catch((err) => {
        isLoading = false;
        dispatch({ type: TYPES_LOADING.LOADING, payload: isLoading });
        console.warn(err);
      });
  };
}
