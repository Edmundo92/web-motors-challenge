import { MARK_TYPES, MarkProps } from "./mark-types";

const INITIAL_STATE: MarkProps[] = [];

type ActionProps = {
  type: string;
  payload: any;
};

function markReducer(state = INITIAL_STATE, action: ActionProps) {
  switch (action.type) {
    case MARK_TYPES.GET_MARK:
      return [...action.payload];
    default:
      return state;
  }
}

export default markReducer;
