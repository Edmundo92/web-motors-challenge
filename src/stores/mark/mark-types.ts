export const MARK_TYPES = {
  GET_MARK: "GET_MARK"
};

export type MarkProps = {
  ID: number | null;
  Name: string | null;
};
