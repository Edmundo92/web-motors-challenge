import { Dispatch } from "redux";
import { MODEL_TYPES } from "./model-types";
import api from "../../services/api";
import { TYPES_LOADING } from "../loading/loading-types";

export const modelAction = (markId: number) => {
  let isLoading = true;

  return (dispatch: Dispatch) => {
    dispatch({ type: TYPES_LOADING.LOADING, payload: isLoading });
    api
      .get(`/OnlineChallenge/Model?MakeID=${markId}`)
      .then((res) => {
        isLoading = false;
        dispatch({ type: TYPES_LOADING.LOADING, payload: isLoading });
        dispatch({
          type: MODEL_TYPES.GET_MODEL,
          payload: res.data
        });
      })
      .catch((err) => {
        isLoading = false;
        dispatch({ type: TYPES_LOADING.LOADING, payload: isLoading });
        console.warn(err);
      });
  };
};
