import { MODEL_TYPES, ModelProps } from "./model-types";

const INITIAL_STATE: ModelProps[] = [];

type ActionProps = {
  type: string;
  payload: any;
};

function modelReducer(state = INITIAL_STATE, action: ActionProps) {
  switch (action.type) {
    case MODEL_TYPES.GET_MODEL:
      return [...action.payload];
    default:
      return state;
  }
}

export default modelReducer;
