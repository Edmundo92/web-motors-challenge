export const MODEL_TYPES = {
  GET_MODEL: "GET_MODEL"
};

export type ModelProps = {
  ID: number | null;
  Name: string;
  MakeID: number | null;
};
