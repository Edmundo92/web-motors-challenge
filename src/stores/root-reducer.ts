import { combineReducers } from "redux";

// reducers
import error from "./error/error-reducer";
import loading from "./loading/loading-reducer";
import markReducer from "./mark/mark-reducer";
import modelReducer from "./model/model-reducer";
import versionReducer from "./version/version-reducer";
import vehicleReducer from "./vehicle/vehicle-reducer";

const rootReducer = combineReducers({
  error,
  loading,
  modelList: modelReducer,
  markList: markReducer,
  vehicleList: vehicleReducer,
  versionList: versionReducer
});

export default rootReducer;
