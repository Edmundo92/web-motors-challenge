import { Dispatch } from "redux";
import api from "../../services/api";
import { VEHICLE_TYPES } from "./vehicle-types";
import { TYPES_LOADING } from "./../loading/loading-types";

export const vehicleAction = () => {
  let isLoading = true;

  return (dispatch: Dispatch) => {
    dispatch({ type: TYPES_LOADING.LOADING, payload: isLoading });
    api
      .get(`/OnlineChallenge/Vehicles?Page=1`)
      .then((res) => {
        isLoading = false;
        dispatch({ type: TYPES_LOADING.LOADING, payload: isLoading });
        dispatch({
          type: VEHICLE_TYPES.GET_VEHICLE,
          payload: res.data
        });
      })
      .catch((err) => {
        isLoading = false;
        dispatch({ type: TYPES_LOADING.LOADING, payload: isLoading });
        console.warn(err);
      });
  };
};
