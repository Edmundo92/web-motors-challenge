import { VEHICLE_TYPES, VehicleProps } from "./vehicle-types";

const INITIAL_STATE: VehicleProps[] = [];

type ActionProps = {
  type: string;
  payload: any;
};

function vehicleReducer(state = INITIAL_STATE, action: ActionProps) {
  switch (action.type) {
    case VEHICLE_TYPES.GET_VEHICLE:
      return [...action.payload];
    default:
      return state;
  }
}

export default vehicleReducer;
