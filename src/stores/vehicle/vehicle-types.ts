export const VEHICLE_TYPES = {
  GET_VEHICLE: "GET_VEHICLE"
};

export type VehicleProps = {
  ID: number;
  Make: string;
  Model: string;
  Version: string;
  Image: string;
  KM: number;
  Price: string;
  YearModel: number;
  YearFab: number;
  Color: string;
};
