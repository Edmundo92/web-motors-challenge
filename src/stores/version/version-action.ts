import { Dispatch } from "redux";
import { VERSION_TYPES } from "./version-types";
import api from "../../services/api";
import { TYPES_LOADING } from "../loading/loading-types";

export const versionAction = (modelId: number) => {
  let isLoading = true;

  return (dispatch: Dispatch) => {
    dispatch({ type: TYPES_LOADING.LOADING, payload: isLoading });
    api
      .get(`/OnlineChallenge/Version?ModelID=${modelId}`)
      .then((res) => {
        isLoading = false;
        dispatch({ type: TYPES_LOADING.LOADING, payload: isLoading });
        dispatch({
          type: VERSION_TYPES.GET_VERSION,
          payload: res.data
        });
      })
      .catch((err) => {
        isLoading = false;
        dispatch({ type: TYPES_LOADING.LOADING, payload: isLoading });
        console.warn(err);
      });
  };
};
