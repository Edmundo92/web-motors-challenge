import { VERSION_TYPES, VersionProps } from "./version-types";

const INITIAL_STATE: VersionProps[] = [];

type ActionProps = {
  type: string;
  payload: any;
};

function versionReducer(state = INITIAL_STATE, action: ActionProps) {
  switch (action.type) {
    case VERSION_TYPES.GET_VERSION:
      return [...action.payload];
    default:
      return state;
  }
}

export default versionReducer;
