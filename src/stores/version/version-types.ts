export const VERSION_TYPES = {
  GET_VERSION: "GET_VERSION"
};

export type VersionProps = {
  ID: number;
  Name: string;
  ModelID: number;
};
